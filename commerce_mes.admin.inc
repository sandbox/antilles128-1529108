<?php

/**
 * @file
 * Administration forms for the Commerce Merchant e-Solutions.
 */

/**
 * Menu callback; Displays the administration settings for Merchant e-Solutions Payment.
 */
function commerce_mes_admin_settings() {
  $form = array();

$form['mes_api_id_key'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Profile ID and Profile Key'),
    '#description' => t('The Profile ID and Key are obtained from your MeS VAR form, Certification Manager, or sales rep.'),
  );
  $form['mes_api_id_key']['commerce_mes_api_profile_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile ID'),
    '#default_value' => variable_get('commerce_mes_api_profile_id', ''),
  );
  $form['mes_api_id_key']['commerce_mes_api_profile_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile Key'),
    '#default_value' => variable_get('commerce_mes_api_profile_key', ''),
  );

  $form['mes_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('MES settings'),
    '#description' => t('These settings pertain to the MeS Payment Gateway.'),
  );
  $form['mes_settings']['commerce_mes_txn_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#description' => t('Live or Simulated transactions.<br/>Be sure to switch to live transactions when testing is done.'),
    '#options' => array(
      'live' => t('Live transactions'),
      'cert' => t('Simulated transactions'),
    ),
    '#default_value' => variable_get('commerce_mes_txn_mode', 'cert'),
  );

  $form['mes_settings']['commerce_mes_response_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log full API response messages for debugging?'),
    '#default_value' => variable_get('commerce_mes_response_debug', FALSE),
  );

  return system_settings_form($form);
}

?>